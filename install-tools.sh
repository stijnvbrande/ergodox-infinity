#!/usr/bin/env bash

sudo apt-get install dfu-util
sudo curl -L https://raw.githubusercontent.com/kiibohd/controller/master/98-kiibohd.rules -o /etc/udev/rules.d/98-kiibohd.rules
curl -L https://github.com/kiibohd/configurator/releases/download/v1.1.0/kiibohd-configurator-1.1.0-linux-x86_64.AppImage -o "${HOME}/bin/kiibohd-configurator-1.1.0-linux-x86_64.AppImage"
chmod a+x "${HOME}/bin/kiibohd-configurator-1.1.0-linux-x86_64.AppImage"
